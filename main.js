const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const emailValidator = require('deep-email-validator');

const port = process.env.PORT || 3030;
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// app.get('/', (req, res) => {
//     res.send('Hello World, from express');
// });

app.post('/check', async (req, res) => {
    const {email} = req.body;
    const checked = await isEmailValid(email)
    console.log(checked.valid)
    res.send({
        isValid: checked.valid,
        reason: checked.reason,
        message: checked.validators.smtp.reason
    });
});

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))

async function isEmailValid(email) {
    return emailValidator.validate(email)
}
